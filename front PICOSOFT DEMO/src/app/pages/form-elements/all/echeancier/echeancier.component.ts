import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import {ValidationsService} from '../../validations/validations.service';
import {AllService} from '../../all.service';
import {SubjService} from '../../subj.service';
import {Gestion2Component} from '../../gestion/gestion2/gestion2.component';

@Component({
  selector: 'app-echeancier',
  templateUrl: './echeancier.component.html',
  styleUrls: ['./echeancier.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EcheancierComponent implements OnInit {

  dataSource: any;
  dataSource2:any;
  data:any;
  assureChoosen:any;

  constructor(private modalService: NgbModal,private subjService :SubjService,public activeModal: NgbActiveModal, public toastrService: ToastrService,
              public validationsService:ValidationsService,
              public allService:AllService) { }
  ngOnInit() {
    this.assureChoosen = this.data.demande.assure;

    this.allService.getEchancierByCredit(this.data.id).subscribe((result:any)=>{
      result.map(item => {
        item.credit.demande.assure.nomprenom = item.credit.demande.assure.nom + ' ' + item.credit.demande.assure.prenom;
        item.montantRemboursement = Number(item.montantRemboursement.replace(/\s/g, ''));
        item.credit.total = Number(item.credit.total.replace(/\s/g, ''));
        item.mensualite = Number(item.mensualite.replace(/\s/g, ''));

        return item;
      });
      this.dataSource2 = result;
    });

    this.validationsService.getCreditByid(this.assureChoosen.id).subscribe(result => {
      this.dataSource = result;
    });


  }
  clear() {
    this.activeModal.dismiss('cancel');
  }

  openModalConsult(data) {
    const modalRef = this.modalService.open(Gestion2Component, {size: 'lg', backdrop: 'static', keyboard: false});
    modalRef.componentInstance.data = data;
  }

}
