import {ValidationsService} from '../validations/validations.service';
import {Component, OnInit, ViewEncapsulation, ViewChild} from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {EcheancierComponent} from 'app/pages/form-elements/all/echeancier/echeancier.component';
import {DxDataGridComponent} from 'devextreme-angular';
import {AllService} from '../all.service';
import {CookieService} from 'angular2-cookie/core';
import {SubjService} from '../subj.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AllComponent implements OnInit {
  dataSource: any;
  arr: any;
  @ViewChild(DxDataGridComponent) dataGrid: DxDataGridComponent;

  constructor(private subjService: SubjService, private modalService: NgbModal, public allService: AllService, private _cookieService: CookieService) {
  }

  ngOnInit() {
    this.getAll();
    this.subjService.testSubject.subscribe(
      (data) => {
        this.getAll()
      }
    )
  }

  getAll() {
    this.allService.getCredit().subscribe((result:any) => {
      result.map(item => {
        item.total = Number(item.total.replace(/\s/g, ''));
        item.mensualite = Number(item.mensualite.replace(/\s/g, ''));

        return item;
      });
      this.dataSource = result;
    });
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift({
      location: 'after',
      widget: 'dxSelectBox',
      options: {
        width: 500,
        items: [{}],
        displayExpr: 'text',
        valueExpr: 'value',
        value: '',
        onValueChanged: ''//this.groupChanged.bind(this)
      }
    }, {
      location: 'after',
      widget: 'dxButton',
      options: {
        icon: 'save',
        onClick: this.filtergrid.bind(this)
      }
    }, {
      location: 'after',
      widget: 'dxButton',
      options: {
        icon: 'refresh',
        onClick: this.getAll.bind(this)
      }
    });
  }

  refreshDataGrid() {
    this.dataGrid.instance.refresh();
  }

  filtergrid() {
    this.dataGrid.instance.getCombinedFilter();
    console.log(this.dataGrid.instance.getCombinedFilter());
    console.log(typeof(this.dataGrid.instance.getCombinedFilter()));

    this.arr = this.dataGrid.instance.getCombinedFilter();
    console.dir(this.arr);

    this._cookieService.put('test', 'test');
    console.log('Set Test Cookie as Test');

  }

  openModalConsult(data) {
    const modalRef = this.modalService.open(EcheancierComponent, {size: 'lg', backdrop: 'static', keyboard: false});
    modalRef.componentInstance.data = data;
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }

}

