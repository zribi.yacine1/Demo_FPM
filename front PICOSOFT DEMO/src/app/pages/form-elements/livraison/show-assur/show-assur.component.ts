import {ToastrService} from 'ngx-toastr';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {VerifierAssureeService} from '../../validations/verifier-assuree/verifier-assuree.service';
import {ValiderLivraisonService} from '../valider-livraison/valider-livraison.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'app-show-assur',
  templateUrl: './show-assur.component.html',
  styleUrls: ['./show-assur.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShowAssurComponent implements OnInit {

  dataSource:any;
  dataSource2:any;
  data: any;
  idAssuree: string
  tel: string;
  nom: string;
  prenom: string;
  naissance: string;
  sexe: string;
  address: string;
  cni: string;
  etat: string;
  fonction: string;
  anl: boolean=false;
  pi: boolean=false;
  ctp: boolean=false;
  cam: boolean=false;
  cdbs: boolean=false;
  ae: boolean=false;  
   constructor(private validerLivraisonService:ValiderLivraisonService, private verifierAssureeService: VerifierAssureeService,public activeModal: NgbActiveModal, public toastrService: ToastrService) { }
 
   ngOnInit() {
     this.idAssuree = this.data.idassuree;
     this.tel = this.data.tel;
     this.nom=this.data.nom;
     this.prenom=this.data.prenom;
     this.naissance = this.data.naissance;
     this.sexe=this.data.sexe;
     this.address=this.data.address
     this.cni = this.data.cni;
     this.etat=this.data.etat;
     this.fonction=this.data.fonction
     this.anl = this.data.anl;
     this.pi=this.data.pi;
     this.ctp=this.data.ctp;
     this.cam = this.data.cam;
     this.cdbs=this.data.cdbs;
     this.ae=this.data.ae;
 
     this.dataSource = this.data.enfant;
     this.dataSource2 = this.data.conjoint;
 
   }
   verifierDemande(assuree){
    let assureeToUpdate = assuree;
    assureeToUpdate.livre = true;
    
  console.log(assureeToUpdate)
    this.validerLivraisonService.updateAssuree(assureeToUpdate).subscribe(result=>{},
      error =>{
        setTimeout(() => {
          this.toastrService.success('Carte livrée avec succés!');
    
        })
      });
 
    this.activeModal.dismiss(true);
  }
   clear() {
     this.activeModal.dismiss('cancel');
   }


}
