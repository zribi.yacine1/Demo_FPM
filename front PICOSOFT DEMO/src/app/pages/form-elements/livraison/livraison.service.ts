import {Observable} from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable()
export class LivraisonService {

  urlAssuree = environment.SERVER_API_URL+ 'api/updateAssuree';

  constructor(private http: HttpClient) { }
  updateAssuree(data): Observable<any> {
    return this.http.post(this.urlAssuree + '.json', data);
    }
}
