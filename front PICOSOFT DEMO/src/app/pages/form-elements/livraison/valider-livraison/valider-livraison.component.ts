import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ValiderLivraisonService } from 'app/pages/form-elements/livraison/valider-livraison/valider-livraison.service';

@Component({
  selector: 'app-valider-livraison',
  templateUrl: './valider-livraison.component.html',
  styleUrls: ['./valider-livraison.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ValiderLivraisonComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal, private validerLivraisonService: ValiderLivraisonService) { }
  ngOnInit() {
  }

  verifierDemande(assuree){
    let assureeToUpdate = assuree;
    assureeToUpdate.livre = true;
    
  console.log(assureeToUpdate)
    this.validerLivraisonService.updateAssuree(assureeToUpdate).subscribe();
    this.activeModal.dismiss(true);
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }

}
