import {ToastrService} from 'ngx-toastr';
import {VerifierAssureeService} from '../verifier-assuree/verifier-assuree.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AfterViewChecked, Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ValidationsService} from '../validations.service';
import {DxDataGridComponent} from 'devextreme-angular';
import {NgForm} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {SubjService} from '../../subj.service';

@Component({
  selector: 'app-show-assuree',
  templateUrl: './show-assuree.component.html',
  styleUrls: ['./show-assuree.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers:[DatePipe]
})
export class ShowAssureeComponent implements OnInit {

  @ViewChild('select2') select2;
  listAssures:any;
  assureChoosen:any;
  dataSource:any;
  demande:any;
  @ViewChild('f') signupForm: NgForm;
  today:any = Date.now();
  credit:any;
  /*dataSource:any;
  dataSource2:any;
  data: any;
  idAssuree: string
  tel: string;
  nom: string;
  prenom: string;
  naissance: string;
  sexe: string;
  address: string;
  cni: string;
  etat: string;
  fonction: string;
  anl: boolean=false;
  pi: boolean=false;
  ctp: boolean=false;
  cam: boolean=false;
  cdbs: boolean=false;
  ae: boolean=false;  */


  constructor(private subjService :SubjService,private datePipe: DatePipe,public activeModal: NgbActiveModal, public toastrService: ToastrService, public validationsService:ValidationsService) {
    this.today = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.assureChoosen = {
      'numSouscriptionFC': '',
      'matricule': '',
      'nom': '',
      'prenom': '',
      'dateNaissance': '',
      'lieuNaissance': '',
      'sexe': '',
      'armee': '',
      'dateEntreService': '',
      'unite': '',
      'grade': '',
      'numSouscriptionMaladie': '',
      'cotisationBruteMaladie': '',
      'plafondMaladie': '',
      'NumSouscriptionFC': '',
      'cotisationBruteFC': '',
      'plafondFC': '',
      'soldeDeBase': '',
      'quotite': '',
      'possibilite': ''
    };

    this.demande = {
      "numSouscription": "",
      "mensualite": "",
      "moisDus": 0,
      "total": "",
      "etat": "",
      "dateDebutRembourcement": "",
      "assure":{
        "id":null
      }
    };
    this.validationsService.getAllAssure().subscribe(result=>{
      this.listAssures = result;
    });
  }

  changeSelect2(){
    if(this.assureChoosen == null){
      this.assureChoosen = {
        'numSouscriptionFC': '',
        'matricule': '',
        'nom': '',
        'prenom': '',
        'dateNaissance': '',
        'lieuNaissance': '',
        'sexe': '',
        'armee': '',
        'dateEntreService': '',
        'unite': '',
        'grade': '',
        'numSouscriptionMaladie': '',
        'cotisationBruteMaladie': '',
        'plafondMaladie': '',
        'NumSouscriptionFC': '',
        'cotisationBruteFC': '',
        'plafondFC': '',
        'soldeDeBase': '',
        'quotite': '',
        'possibilite': ''
      };
      this.dataSource = null;
    }else {
      this.validationsService.getCreditByid(this.assureChoosen.id).subscribe(result=>{
        this.dataSource = result;
      })
    }
  }

  addDemande(){
    this.demande.assure.id = this.assureChoosen.id;
    this.validationsService.addDemande(this.demande).subscribe(result=>{
      this.subjService.testSubject.next('a new data from BOUTON');
      this.activeModal.dismiss('cancel');

    })
  }



  ngOnInit() {
   /* this.idAssuree = this.data.idassuree;
    this.tel = this.data.tel;
    this.nom=this.data.nom;
    this.prenom=this.data.prenom;
    this.naissance = this.data.naissance;
    this.sexe=this.data.sexe;
    this.address=this.data.address
    this.cni = this.data.cni;
    this.etat=this.data.etat;
    this.fonction=this.data.fonction
    this.anl = this.data.anl;
    this.pi=this.data.pi;
    this.ctp=this.data.ctp;
    this.cam = this.data.cam;
    this.cdbs=this.data.cdbs;
    this.ae=this.data.ae;

    this.dataSource = this.data.enfant;
    this.dataSource2 = this.data.conjoint;
*/
  }
  /*verifierDemande(assuree){
    let assureeToUpdate = assuree;
    assureeToUpdate.valide = true;

  console.log(assureeToUpdate)
    this.verifierAssureeService.updateAssuree(assureeToUpdate).subscribe(result=>{},
      error =>{
        setTimeout(() => {
          this.toastrService.success('Demande d\'immatriculation acceptée!');

        })
      }
  );
    this.activeModal.dismiss(true);
  }*/
  clear() {
    this.activeModal.dismiss('cancel');
  }
}
