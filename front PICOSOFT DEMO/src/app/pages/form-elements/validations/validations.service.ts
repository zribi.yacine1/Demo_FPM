import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient } from "@angular/common/http";


@Injectable()
export class ValidationsService {
  url = environment.SERVER_API_URL+ 'api/demandes';
  urlAssuree = environment.SERVER_API_URL+ 'api/assurees';
    urlEnfants = environment.SERVER_API_URL+ 'api/enfants';
    urlConjoints = environment.SERVER_API_URL+ 'api/conjoints';

    constructor(private http: HttpClient) { }

    getvalidation() {
        return this.http.get(this.urlAssuree).map(res =>  JSON.parse(JSON.stringify(res)));
    }
    getEnfants(){
        return this.http.get(this.urlEnfants).map(res =>  JSON.parse(JSON.stringify(res)));
    }
    getConjoints(){
        return this.http.get(this.urlConjoints).map(res =>  JSON.parse(JSON.stringify(res)));
    }

  getDemandes() {
    return this.http.get(this.url);
  }

  getAllAssure(){
    return this.http.get(environment.SERVER_API_URL+ 'api/assures');
  }

  getCreditByid(id){
    return this.http.get(environment.SERVER_API_URL+ 'api/creditsByIdAssure/'+id);
  }

  addDemande(demande){
    return this.http.post(environment.SERVER_API_URL+ 'api/demandes',demande);
  }

  updateDemande(demande){
    return this.http.put(environment.SERVER_API_URL+ 'api/demandes',demande);
  }

  addCredit(credit){
    return this.http.post(environment.SERVER_API_URL+ 'api/credits',credit);
  }

  sendSms(to,subject,body){
      return this.http.post(environment.SERVER_API_URL + 'api/send?to='+to+'&subject='+subject+'&body='+body, null)
  }

  getCreditByDemande(idDemande){
      return this.http.get(environment.SERVER_API_URL +'api/creditsByIdDemande/'+idDemande)
  }
}
