import {ToastrService} from 'ngx-toastr';
import { Component, ViewEncapsulation } from '@angular/core';
import { EditorService } from 'app/pages/form-elements/editor/editor.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  encapsulation: ViewEncapsulation.None
})
export class EditorComponent{
  
  idAssuree: string;
  tel:string;
  nom: string;
  prenom: string;
  naissance: string;
  sexe: string;
  fonction: string;
  anl: boolean=false;
  oa: boolean=false;
  bas: boolean=false;

  constructor(private editorService: EditorService, public toastrService: ToastrService) { }

  ngOnInit() { }

  addEnfant() {

    let enfant = null;
    enfant = {
      "idAss" :this.idAssuree.toString(),
      "tel":this.tel,
      "nom": this.nom,
      "prenom": this.prenom,
      "naissance": this.naissance,
      "sexe": this.sexe,
      "anl": this.anl,
      "oa": this.oa,
      "bas": this.bas,
      "valideEnfant":false,
      "livreEnfant":false,
      "prodEnfant":false
    }

    console.log(enfant);

    return this.editorService.registerEnfant(enfant).subscribe(enfant => {

    },
    error =>{
      setTimeout(() => {
        this.toastrService.success('Enfant ajouté avec succés!');
  
      })
    });
  }
}