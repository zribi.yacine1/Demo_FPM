import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import {AllService} from '../../all.service';
import {NgForm} from '@angular/forms';
import {SubjService} from '../../subj.service';

@Component({
  selector: 'app-gestion2',
  templateUrl: './gestion2.component.html',
  styleUrls: ['./gestion2.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Gestion2Component implements OnInit {

  items = ['Attent paiement', 'Rembourcement effectué'];
  @ViewChild('ff') signupForm: NgForm;
  data:any;
  dataSource: any;
  etat:any = '';

  constructor(public activeModal: NgbActiveModal, public toastrService: ToastrService,public allService:AllService,private subjService :SubjService) {
  }
  ngOnInit() {
    this.etat = this.data.etat;
  }
  clear() {
    this.activeModal.dismiss('cancel');
  }

  updateEchancier(){
    this.allService.updateEchancier(this.data.id).subscribe(result=>{
      this.subjService.testSubject.next('a new data from BOUTON');
      this.activeModal.dismiss('cancel');
    })
  }
}
