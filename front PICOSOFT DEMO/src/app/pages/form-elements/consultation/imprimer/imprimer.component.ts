import {VerifierAssureeService} from '../../validations/verifier-assuree/verifier-assuree.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ImprimerService} from './imprimer.service';
import {NgForm} from '@angular/forms';
import {SubjService} from '../../subj.service';


@Component({
  selector: 'app-imprimer',
  templateUrl: './imprimer.component.html',
  styleUrls: ['./imprimer.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ImprimerService]
})
export class ImprimerComponent implements OnInit {
  data: any;
  assure: any;
  @ViewChild('f') signupForm: NgForm;

  constructor(public activeModal: NgbActiveModal, public imprimerService: ImprimerService,private subjService :SubjService) {
  }


  ngOnInit() {
    this.assure = {
      'numSouscriptionFC': '',
      'matricule': '',
      'nom': '',
      'prenom': '',
      'dateNaissance': '',
      'lieuNaissance': '',
      'sexe': '',
      'armee': '',
      'dateEntreService': '',
      'grade': '',
      'numSouscriptionMaladie': '',
      'cotisationBruteMaladie': '',
      'plafondMaladie': '',
      'NumSouscriptionFC': '',
      'cotisationBruteFC': '',
      'plafondFC': '',
      'soldeDeBase': '',
      'quotite': '',
      'possibilite': '',
      'tel':''
    };
  }


  addAssuer() {
    this.imprimerService.addAssuree(this.assure).subscribe(result=>{
      this.activeModal.dismiss('cancel');
      this.subjService.testSubject.next('a new data from BOUTON');

    });
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
