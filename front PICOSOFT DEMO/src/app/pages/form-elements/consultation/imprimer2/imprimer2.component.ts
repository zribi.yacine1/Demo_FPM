import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {NgForm} from '@angular/forms';
import {ImprimerService} from '../imprimer/imprimer.service';


@Component({
  selector: 'app-imprimer2',
  templateUrl: './imprimer2.component.html',
  styleUrls: ['./imprimer2.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [ImprimerService]

})
export class Imprimer2Component implements OnInit {
  data: any;
  assure: any;
  @ViewChild('f') signupForm: NgForm;

  constructor(public activeModal: NgbActiveModal, public imprimerService: ImprimerService) {
  }

  ngOnInit() {
    this.assure = this.data;
  }

  updateAssuer(){
    return this.imprimerService.updateAssure(this.assure).subscribe(result=>{
      this.activeModal.dismiss('cancel');
    });
  }

  addAssuer() {
    this.imprimerService.addAssuree(this.assure).subscribe(result=>{
      this.activeModal.dismiss('cancel');
    });
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
