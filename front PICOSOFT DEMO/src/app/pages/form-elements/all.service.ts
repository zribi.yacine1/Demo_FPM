import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient } from "@angular/common/http";


@Injectable()
export class AllService {

  constructor(private http: HttpClient) { }

  getCredit(){
    return this.http.get(environment.SERVER_API_URL+ 'api/credits');
  }

  getEchancierByCredit(idCredit){
    return this.http.get(environment.SERVER_API_URL+ 'api/echeanciersByIdCredit/'+idCredit);
  }
  getEchancier(){
    return this.http.get(environment.SERVER_API_URL+ "api/echeanciers");
  }

  updateEchancier(idEcheancier){
    return this.http.put(environment.SERVER_API_URL+ "api/echeanciers/"+idEcheancier,null);
  }
}
