import {ToastrService} from 'ngx-toastr';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LayoutService } from 'app/pages/form-elements/layouts/layouts.service';

@Component({
  selector: 'app-layouts',
  templateUrl: './layouts.component.html',
  encapsulation: ViewEncapsulation.None
})
export class LayoutsComponent implements OnInit {
  idAssuree: string;
  
  tel: string;
  nom: string;
  prenom: string;
  naissance: string;
  cni: string;
  fonction: string;
  anl:boolean;
  am: boolean;
  constructor(private layoutService:LayoutService, public toastrService: ToastrService) { }


  ngOnInit() {
  }
  addConjoint() {
    
        let conjoint = null;
        conjoint = {
           "idAss" :this.idAssuree,
          "tel":this.tel,
          "nom": this.nom,
          "prenom": this.prenom,
          "naissance": this.naissance,
          "cni": this.cni,
          "fonction": this.fonction,
          "valideConjoint":false,
          "livreConjoint":false,
          "prodConjoint":false,
          "anl": false,
          "am": false,
          
        }
    
        console.log(conjoint);
    
        return this.layoutService.addConjoint(conjoint).subscribe(conjoint => {

        },
        error =>{
          setTimeout(() => {
            this.toastrService.success('Conjoint ajouté avec succés!');
      
          })
        });
      }

}
