import { environment } from "environments/environment";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class LayoutService {
    url = environment.SERVER_API_URL+ 'api/registerConjoint';

    constructor(private http: HttpClient) { }
    addConjoint(data): Observable<any> {
        return this.http.post(this.url + '.json', data)
            .map(this.extractData);
            // .catch(this.handleError);
    }
    private extractData(res: Response) {
        // tslint:disable-next-line:curly
        if (res)
            return res || {};
        // tslint:disable-next-line:curly
        else
            return null;
    }

    // private handleError(error: Response | any) {
    //     console.error(error.message || error);
    //     return Observable.throw(error.message || error);
    // }

}
