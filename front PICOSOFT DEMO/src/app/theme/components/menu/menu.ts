import { Menu } from './menu.model';

export const verticalMenuItems = [
  new Menu (1, 'Gestion des assurés', '/pages/form-elements/consultation', null, 'user', null, false, 0),
  new Menu (2, 'Gestion des demandes', '/pages/form-elements/validations', null, 'folder-open-o', null, false, 0),
  new Menu (3, 'Gestion des crédits', '/pages/form-elements/all', null, 'dollar', null, false, 0),
  new Menu (4, 'Gestion des échéanciers', '/pages/form-elements/gestion', null, 'money', null, false, 0)
]

export const horizontalMenuItems = [
  new Menu (1, 'Gestion des assurés', '/pages/form-elements/consultation', null, 'user', null, false, 0),
  new Menu (2, 'Gestion des demandes', '/pages/form-elements/validations', null, 'folder-open-o', null, false, 0),
  new Menu (3, 'Gestion des crédits', '/pages/form-elements/all', null, 'dollar', null, false, 0),
  new Menu (4, 'Gestion des échéanciers', '/pages/form-elements/gestion', null, 'money', null, false, 0)
]
