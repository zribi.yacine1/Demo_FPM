package com.example.demo.service;

import com.example.demo.domain.Assure;
import com.example.demo.domain.Demande;
import com.example.demo.domain.Historique;
import com.example.demo.repository.AssureRepository;
import com.example.demo.repository.DemandeRepository;
import com.example.demo.repository.HistoriqueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@Transactional
public class DemandeService {

    @Autowired
    private DemandeRepository demandeRepository;
    @Autowired
    private AssureRepository assureRepository;
    @Autowired
    private HistoriqueRepository historiqueRepository;

    public List<Demande> findByAssure(Long id){
        Assure assure = assureRepository.findOne(id);
        return demandeRepository.findByAssure(assure);
    }

    public List<Demande> findByAssure(String matriculeAssure){
        Assure assure = assureRepository.findByMatricule(matriculeAssure);
        return demandeRepository.findByAssure(assure);
    }

    public Demande create(Demande demande) {
        log.debug("Request to create assure : {}", demande);
        Demande resultDemande = demandeRepository.save(demande);
        historiqueRepository.save(new Historique(null, resultDemande.getId(), "Demande", "admin", "Creation", null));
        return resultDemande;
    }

    public Demande update(Demande demande) {
        log.debug("Request to update assure : {}", demande);

        Demande resultDemande = demandeRepository.save(demande);
        historiqueRepository.save(new Historique(null, resultDemande.getId(), "Demande", "admin", "Modification", null));
        return resultDemande;
    }

    @Transactional(readOnly = true)
    public List<Demande> findAll() {
        log.debug("Request to get all assures");
        return demandeRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Demande findOne(Long id) {
        return demandeRepository.findOne(id);
    }

    public void delete(Long id) {
        log.debug("Request to delete assure : {}", id);
        historiqueRepository.save(new Historique(null, id, "Demande", "admin", "Suppression", null));
        demandeRepository.delete(id);
    }
}
