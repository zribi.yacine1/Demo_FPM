package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Table
@Entity
public class Echeancier implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long num;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd")
    private Calendar dateEcheance;
    private String capitalDu;
    private String interet;
    private String montantRemboursement;
    private String frais;
    private String mensualite;
    private String etat;
    @NotNull
    @ManyToOne
    private Credit credit;
}
