package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Table
@Entity
public class Credit implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String numSouscription;//Numéro de demande
    private String mensualite;
    private Integer moisDus;
    private String total;//montant accordé
    private String etat;
    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Calendar dateDebutRembourcement;
    @NotNull
    @OneToOne
    private Demande demande;
    @JsonIgnore
    @OneToMany(mappedBy = "credit")
    private List<Echeancier> echeanciers;
    @JsonIgnore
    @ManyToOne
    private Assure assure;
}
