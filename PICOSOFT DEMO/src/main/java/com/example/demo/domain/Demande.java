package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Table
@Entity
public class Demande implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private java.util.Calendar datedemande;
    private String montantDemande;
    private String dureeRemboursement;
    private String motifDemande;
    private String typePret;
    private String typeApport;
    private String netApayerSansEngagement;
    private String typeRachat;
    private String methodeCalculeMontantRachat;
    private String decision;
    private String motifDecision;
    @NotNull
    @ManyToOne
    private Assure assure;
    @JsonIgnore
    @OneToOne(mappedBy = "demande", cascade = CascadeType.ALL, orphanRemoval = true)
    private Credit credit;
}
