package com.example.demo.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Table
@Entity
public class Assure implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotNull
    @Column(unique = true)
    private String matricule;
    @NotNull
    private String nom;
    @NotNull
    private String prenom;
    private String dateNaissance;
    private String lieuNaissance;
    private String sexe;
    private String armee;
    private String dateEntreService;
    private String unite;
    private String grade;
    private String numSouscriptionMaladie;
    private String cotisationBruteMaladie;
    private String plafondMaladie;
    private String NumSouscriptionFC;
    private String cotisationBruteFC;
    private String plafondFC;
    private String soldeDeBase;
    private String quotite;
    private String possibilite;
    private String tel;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @JsonIgnore
    @OneToMany(mappedBy = "assure")
    private List<Demande> demandes;
    @JsonIgnore
    @OneToMany(mappedBy = "assure")
    private List<Credit> credit;
}
