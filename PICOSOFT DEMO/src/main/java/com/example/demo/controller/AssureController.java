package com.example.demo.controller;

import com.example.demo.domain.Assure;
import com.example.demo.service.AssureService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api")
public class AssureController {
    private static final String ENTITY_NAME = "Assure";

    @Autowired
    private AssureService assureService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/assures")
    public ResponseEntity<Assure> createAssure(@Valid @RequestBody Assure assure) throws URISyntaxException {
        log.debug("REST request to save Assure : {}", assure);
        if (assure.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new Assure cannot already have an ID")).body(null);
        }
        Assure result = assureService.create(assure);
        return ResponseEntity.created(new URI("/api/assures/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("/assures")
    public ResponseEntity<Assure> updateAssure(@Valid @RequestBody Assure assure) throws URISyntaxException {
        log.debug("REST request to update Assure : {}", assure);
        if (assure.getId() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "Can't find ID")).body(null);
        }
        Assure result = assureService.update(assure);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, assure.getId().toString()))
                .body(result);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/assures")
    public List<Assure> getAllAssure() {
        log.debug("REST request to get all Assures");
        return assureService.findAll();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/assures/{id}")
    public ResponseEntity<Assure> getAssure(@PathVariable Long id) {
        log.debug("REST request to get Assure : {}", id);
        Assure assure = assureService.findOne(id);
        return Optional.ofNullable(assure).map(response -> ResponseEntity.ok().headers(null).body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/assuresByMatricule/{matricule}")
    public ResponseEntity<Assure> findByMatricule(@PathVariable String matricule) {
        log.debug("REST request to get Assure : {}", matricule);
        Assure assure = assureService.findByMatricule(matricule);
        return Optional.ofNullable(assure).map(response -> ResponseEntity.ok().headers(null).body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/assures/{id}")
    public ResponseEntity<Void> deleteAssure(@PathVariable Long id) {
        log.debug("REST request to delete Assure : {}", id);
        assureService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
