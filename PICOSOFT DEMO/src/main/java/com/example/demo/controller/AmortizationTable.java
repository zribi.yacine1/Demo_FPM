package com.example.demo.controller;

import java.text.NumberFormat;

public class AmortizationTable {

    public static void main(String[] args) {

        String principals = "2460593";
        String longueurs = "24";
        String interets = "9.569";

        double capitalDu = Double.parseDouble(principals);
        int length = Integer.parseInt(longueurs);
        double amountInterest = Double.parseDouble(interets);

        double monthlyInterest = amountInterest / (12 * 100);
        double mensualite = capitalDu * (monthlyInterest / (1 - Math.pow((1 + monthlyInterest), (length * -1))));

        System.out.print("mensualite   " + "montantRemboursement   " + "interet   " + "capitalDu   ");
        System.out.println();

        NumberFormat nf = NumberFormat.getIntegerInstance();

        for (int i = 1; i <= length; i++) {
            double interet = capitalDu * monthlyInterest;
            double montantRemboursement = mensualite - interet;
            if (i != 1) {
                capitalDu = capitalDu - montantRemboursement;
            }
            System.out.print(nf.format(mensualite) + "      " + nf.format(montantRemboursement) + "                 " + nf.format(interet) + "    " + nf.format(capitalDu));
            System.out.println();
        }
    }
}