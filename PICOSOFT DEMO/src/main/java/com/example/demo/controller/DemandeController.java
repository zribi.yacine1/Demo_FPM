package com.example.demo.controller;

import com.example.demo.domain.Demande;
import com.example.demo.service.DemandeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api")
public class DemandeController {
    private static final String ENTITY_NAME = "Demande";

    @Autowired
    private DemandeService demandeService;
    @Autowired
    public JavaMailSender emailSender;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/demandes")
    public ResponseEntity<Demande> createDemande(@Valid @RequestBody Demande demande) throws URISyntaxException {
        log.debug("REST request to save Demande : {}", demande);
        if (demande.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new Demande cannot already have an ID")).body(null);
        }
        Demande result = demandeService.create(demande);
        return ResponseEntity.created(new URI("/api/demandes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("/demandes")
    public ResponseEntity<Demande> updateDemande(@Valid @RequestBody Demande demande) throws URISyntaxException {
        log.debug("REST request to update Demande : {}", demande);
        if (demande.getId() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "Can't find ID")).body(null);
        }
        Demande result = demandeService.update(demande);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, demande.getId().toString()))
                .body(result);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/demandes")
    public List<Demande> getAllDemande() {
        log.debug("REST request to get all Demandes");
        return demandeService.findAll();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/demandesByIdAssure/{idAssure}")
    public List<Demande> findByAssure(@PathVariable Long idAssure) {
            log.debug("REST request to get all Demandes By idAssure");
            return demandeService.findByAssure(idAssure);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/demandesByMatriculeAssure/{matriculeAssure}")
    public List<Demande> findByAssure(@PathVariable String matriculeAssure) {
        log.debug("REST request to get all Demandes By idAssure");
        return demandeService.findByAssure(matriculeAssure);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/demandes/{id}")
    public ResponseEntity<Demande> getDemande(@PathVariable Long id) {
        log.debug("REST request to get Demande : {}", id);
        Demande demande = demandeService.findOne(id);
        return Optional.ofNullable(demande).map(response -> ResponseEntity.ok().headers(null).body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/demandes/{id}")
    public ResponseEntity<Void> deleteDemande(@PathVariable Long id) {
        log.debug("REST request to delete Demande : {}", id);
        demandeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @RequestMapping(value = "/send", method = RequestMethod.POST, headers = "Accept=application/json")
    public void sendMail(@RequestParam String to, @RequestParam String subject, @RequestParam String body){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("ms@picosoft.biz");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        emailSender.send(message);
    }
}
