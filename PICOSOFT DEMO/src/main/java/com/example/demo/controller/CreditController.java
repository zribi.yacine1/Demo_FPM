package com.example.demo.controller;

import com.example.demo.domain.Credit;
import com.example.demo.service.CreditService;
import com.example.demo.service.EcheancierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api")
public class CreditController {
    private static final String ENTITY_NAME = "Credit";

    @Autowired
    private CreditService creditService;
    @Autowired
    private EcheancierService echeancierService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping("/credits")
    public ResponseEntity<Credit> createCredit(@Valid @RequestBody Credit credit) throws URISyntaxException {
        log.debug("REST request to save Credit : {}", credit);
        if (credit.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new Credit cannot already have an ID")).body(null);
        }
        Credit result = creditService.create(credit);

        return ResponseEntity.created(new URI("/api/credits/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("/credits")
    public ResponseEntity<Credit> updateCredit(@Valid @RequestBody Credit credit) throws URISyntaxException {
        log.debug("REST request to update Credit : {}", credit);
        if (credit.getId() == null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "Can't find ID")).body(null);
        }
        Credit result = creditService.update(credit);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, credit.getId().toString()))
                .body(result);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/credits")
    public List<Credit> getAllCredits() {
        log.debug("REST request to get all Credits");
        return creditService.findAll();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/credits/{id}")
    public ResponseEntity<Credit> getCredit(@PathVariable Long id) {
        log.debug("REST request to get Credit : {}", id);
        Credit credit = creditService.findOne(id);
        return Optional.ofNullable(credit).map(response -> ResponseEntity.ok().headers(null).body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/creditsByIdAssure/{idAssure}")
    public List<Credit> getCreditsByIdAssure(@PathVariable Long idAssure) {
        log.debug("REST request to get all Credits By idAssure");
        return creditService.findByAssure(idAssure);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/creditsByMatriculeAssure/{matriculeAssure}")
    public List<Credit> getCreditsByIdAssure(@PathVariable String matriculeAssure) {
        log.debug("REST request to get all Credits By matriculeAssure");
        return creditService.findByAssure(matriculeAssure);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("/credits/{id}")
    public ResponseEntity<Void> deleteCredit(@PathVariable Long id) {
        log.debug("REST request to delete Credit : {}", id);
        creditService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping("/creditsByIdDemande/{idDemande}")
    public Credit getCreditByIdDemande(@PathVariable Long idDemande) {
        log.debug("REST request to get all Credits By idDemande");
        return creditService.findByDemande(idDemande);
    }
}
