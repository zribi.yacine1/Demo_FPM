package com.example.demo.repository;

import com.example.demo.domain.Assure;
import com.example.demo.domain.Credit;
import com.example.demo.domain.Demande;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CreditRepository extends JpaRepository<Credit, Long> {
    List<Credit> findByAssure(Assure assure);
    Credit findByDemande(Demande demande);
}