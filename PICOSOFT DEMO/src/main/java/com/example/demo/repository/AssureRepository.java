package com.example.demo.repository;

import com.example.demo.domain.Assure;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssureRepository extends JpaRepository<Assure, Long> {
    Assure findByMatricule(String matricule);

}